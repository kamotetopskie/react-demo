import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';

class App extends Component {
  state = {
    persons: [
      { id: 'asdasd', name: 'Hermil', age: 28 },
      { id: 'asgrywe', name: 'Gil', age: 29 },
      { id: 'sdfxczv', name: 'Test', age: 30 }
    ],
    showPersons: false
  }

  switchNameHandler = (newName) => {
      //console.log('Was clicked!');
      //this.state.persons[1].name = 'Kamote';
      this.setState({
        persons: [
          { name: newName, age: 28 },
          { name: 'Gil', age: 29 },
          { name: 'Test', age: 32 }
        ]
      });
  }

  changeNameHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p =>{
      return p.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
      //This, we use the SPREAD operator to merge the value of original object to new object since Objects and Array are reference type only
    };

    //const person = Object.assign({}, this.state.persons[personIndex]); // -> This is the alternative way of assigning the value of an objet to a new object using vanilla JS

    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({persons: persons});
  }

  deletePersonHandler = (personIndex) => {
    const persons = this.state.persons;
    persons.splice(personIndex, 1);
    this.setState({persons: persons})
  }

  togglePersonsHandler = (event) => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons: !doesShow}); //We will set the showPersons when doesShow is NOT (If the doesShow is TRUE the showPersons will become false vice versa). This is gonna be merged w/ other state.
  }

  render() {
    const style = {
      backgroundColor: 'green',
      color: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer'
    };

    let persons = null;
    
    if (this.state.showPersons) {
      persons = (
        <div>
          {this.state.persons.map((person, index) => { //This map method returns new array
              return <Person 
                click={() => this.deletePersonHandler(index)}
                name={person.name} 
                age={person.age}//You must return what you want to map this item into.
                key={person.id}
                changed={(event) => this.changeNameHandler(event, person.id)}/>
          })}
        </div>
      );
      style.backgroundColor = 'red';
    }

    //let classes = ['red', 'bold'].join(' '); //This will join the array with an empty space between "red bold"
    
    let classes = [];
    if (this.state.persons.length <= 2) {
      classes.push('red'); //classes = ['red']
    }

    if (this.state.persons.length <= 1) {
      classes.push('bold'); //classes = ['red', 'bold']
    }

    return (
      <div className="App">
        <h1>Hi, I'm a React App</h1>
        <p className={classes.join(' ')}>This is really working!</p>
        <button style={style} onClick={this.togglePersonsHandler}>Toggle Persons</button>
        {persons}
      </div>
    );
  }
}

export default App;